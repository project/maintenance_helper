INTRODUCTION
-----------------
The maintenance helper is intended to provide a set of tools useful to admins
while upgrading a site. By providing a permission to login to sites, the module
allows admins to keep their site visible to users while preventing users from
logging in and creating content or posting comments. This is a more flexible
solution than simply putting a site into maintenance mode.

The module optionally displays a message to end users which can be used to
announce, for example, "If you see this message you are looking at the site on
the old server and will not be able to login. Once this message disappears that
means your DNS is updated to the new server and you will be able to login."

INSTALLATION
----------------

1. Copy the files to your sites/all/modules directory.

2. Enable the Maintenance Helper module at Administer > Site building > Modules

3. The permission to login will be enforce immediately after installation, so
you will probably want to immediately go to Administer > User management >
Permissions and grant "login to site" permission to most of the roles on your
site.

FEEDBACK, SUPPORT, CREDIT
---------------------------------
View bugs and feature requestsfor the module at
http://drupal.org/project/issues/maintenance_helper where you can also submit a
bug or feature request.

Developed by Greg Knaddison - greggles http://drupal.org/user/36762 