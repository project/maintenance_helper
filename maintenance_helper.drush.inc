<?php

/**
 * @file
 * Drush integration functions for the maintenance_helper module.
 */

/**
 * Implements hook_drush_command().
 */
function maintenance_helper_drush_command() {
  $items['mh-restrict-logins'] = array(
    'callback' => 'maintenance_helper_drush_set_restrict_logins',
    'description' => dt("Enable or disable restricted logins.\nUsers without the 'login during restricted logins' will be logged out."),
    'drupal dependencies' => array('maintenance_helper'),
    'arguments' => array(
      'setting' => dt('1 to enable restricted logins, 0 to disable'),
    ),
    'examples' => array(
      'drush mh-restrict-logins 1' => dt('Enable restricted logins'),
      "drush mh-restrict-logins 1 --message='Go away' --show-message=1" => dt("Enable restricted logins and show the message 'Go Away'"),
    ),
    'options' => array(
      '--message' => dt('The message to show logged in users. Default value is the last message used.'),
      '--show-message' => dt('Whether to show the message. 1 to show on all pages, 0 to not show.'),
    ),
  );
  return $items;
}

/**
 * Set or unset retrict logins
 */
function maintenance_helper_drush_set_restrict_logins() {
  $restrict_logins_setting = current(func_get_args());
  $message = drush_get_option('message');
  $show_message = drush_get_option('show-message');

  if ($restrict_logins_setting === '1') {
    $restrict_logins = true;
  }
  else if ($restrict_logins_setting === '0') {
    $restrict_logins = false;
  }
  else {
    drush_show_help('mh-restrict-logins');
    return;
  }

  if (isset($message)) {
    maintenance_helper_set_message($message);
    drush_print(dt("Message set to '$message'."));
  }

  if (isset($show_message)) {
    maintenance_helper_set_show_message($show_message === "1");
    drush_print(dt("Show-message set to $show_message."));
  }

  maintenance_helper_set_restrict_logins($restrict_logins);
  $state = ($restrict_logins ? 'enabled' : 'disabled');
  drush_print(dt("restricted logins $state."));
}
